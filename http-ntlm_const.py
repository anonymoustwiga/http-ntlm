# File: pcapreader_consts.py
# Copyright (c) Splunk
#
# CONFIDENTIAL - Use or disclosure of this material in whole or in part
# without a valid written license from the autor is PROHIBITED.

PCAPREADER_FILE_NOT_IN_VAULT = 'Could not find specified vault ID in vault'
