# File: pcapreader_connector.py
# Copyright (c) Splunk
#
# CONFIDENTIAL - Use or disclosure of this material in whole or in part
# without a valid written license from the autor is PROHIBITED.

# Phantom App imports
import phantom.app as phantom
from phantom.base_connector import BaseConnector
from phantom.action_result import ActionResult

# Usage of the consts file is recommended
import requests
import json
from bs4 import BeautifulSoup
# from base64 import b64encode
from requests_ntlm import HttpNtlmAuth


class RetVal(tuple):
    def __new__(cls, val1, val2=None):
        return tuple.__new__(RetVal, (val1, val2))


class HTTPNTLMConnector(BaseConnector):

    def __init__(self):

        # Call the BaseConnectors init first
        super(HTTPNTLMConnector, self).__init__()

        self._state = None

        # Variable to hold a base_url in case the app makes REST calls
        # Do note that the app json defines the asset config, so please
        # modify this as you deem fit.
        self._base_url = None

    def _process_empty_response(self, response, action_result):

        if response.status_code == 200:
            return RetVal(phantom.APP_SUCCESS, {})

        return RetVal(action_result.set_status(phantom.APP_ERROR, "Empty response and no information in the header"), None)

    def _process_html_response(self, response, action_result):
        # An html response, treat it like an error
        self.debug_print("Received HTML response!")
        status_code = response.status_code

        try:
            soup = BeautifulSoup(response.text, "html.parser")
            error_text = soup.text
            split_lines = error_text.split('\n')
            split_lines = [x.strip() for x in split_lines if x.strip()]
            error_text = '\n'.join(split_lines)
        except Exception as e:
            self.debug_print("ERROR in _process_json_response: {}".format(str(e)))
            error_text = "Cannot parse error details"

        message = "Status Code: {0}. Data from server:\n{1}\n".format(status_code,
                error_text)
        self.debug_print(message)
        message = message.replace(u'{', '{{').replace(u'}', '}}')

        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _process_json_response(self, r, action_result):

        # Try a json parse
        try:
            resp_json = r.json()
            self.debug_print("response json: {}".format(resp_json))

        except Exception as e:
            self.debug_print("ERROR in _process_json_response: {}".format(str(e)))
            return RetVal(action_result.set_status(phantom.APP_ERROR, "Unable to parse JSON response. Error: {0}".format(str(e))), None)

        # Please specify the status codes here
        if 200 <= r.status_code < 399:
            # return RetVal(phantom.APP_SUCCESS, resp_json)
            return RetVal(action_result.set_status(phantom.APP_SUCCESS, json.dumps(resp_json)), resp_json)

        # You should process the error returned in the json
        message = "Error from server. Status Code: {0} Data from server: {1}".format(
                r.status_code, r.text.replace(u'{', '{{').replace(u'}', '}}'))

        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _process_response(self, r, action_result):

        # store the r_text in debug data, it will get dumped in the logs if the action fails
        if hasattr(action_result, 'add_debug_data'):
            action_result.add_debug_data({'r_status_code': r.status_code})
            action_result.add_debug_data({'r_text': r.text})
            action_result.add_debug_data({'r_headers': r.headers})

        # debug information
        self.debug_print("status code: {}".format(r.status_code))
        self.debug_print("r_text: {}".format(r.text))

        # Extract the headers
        self.debug_print("response_headers")
        self.debug_print(r.headers)
        # action_result.add_data({ 'response_headers': r.headers })

        # Process each 'Content-Type' of response separately

        # Process a json response
        if 'json' in r.headers.get('Content-Type', ''):
            return self._process_json_response(r, action_result)

        # Process an HTML response, Do this no matter what the api talks.
        # There is a high chance of a PROXY in between phantom and the rest of
        # world, in case of errors, PROXY's return HTML, this function parses
        # the error and adds it to the action_result.
        if 'html' in r.headers.get('Content-Type', ''):
            return self._process_html_response(r, action_result)

        # it's not content-type that is to be parsed, handle an empty response
        if not r.text:
            # return self._process_text_response(r, action_result)
            return self._process_empty_response(r, action_result)

        # everything else is actually an error at this point
        message = "Can't process response from server. Status Code: {0} Data from server: {1}".format(
                r.status_code, r.text.replace('{', '{{').replace('}', '}}'))
        self.debug_print("message: {}".format(message))

        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _make_rest_call(self, endpoint, action_result, method="get", **kwargs):
        # **kwargs can be any additional parameters that requests.request accepts

        config = self.get_config()

        resp_json = None

        try:
            request_func = getattr(requests, method)
        except AttributeError as e:
            self.debug_print("ERROR in _make_rest_call: {}".format(str(e)))
            return RetVal(action_result.set_status(phantom.APP_ERROR, "Invalid method: {0}".format(method)), resp_json)

        # Create a URL to connect to
        # change to source from parameter
        # url

        url = self._base_url + endpoint

        try:
            r = request_func(
                            url,
                            # auth=(username, password),  # basic authentication
                            verify=config.get('verify_server_cert', True),
                            **kwargs)
        except Exception as e:
            self.debug_print("ERROR in _make_rest_call: {}".format(str(e)))
            return RetVal(action_result.set_status( phantom.APP_ERROR, "Error Connecting to server. Details: {0}".format(str(e))), resp_json)

        return self._process_response(r, action_result)

    def _handle_http_get(self, param):
        self.save_progress("In action handler for: {0}".format(self.get_action_identifier()))

        # add return struction
        action_result = self.add_action_result(ActionResult(dict(param)))

        # get the url
        location = param.get('location', "")
        self.debug_print("location parameter: {}".format(location))

        # get the username
        # username = param['username']
        username = self.get_config().get('username')

        # get the password
        # password = param['password']
        password = self.get_config().get('password')

        request_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        self.debug_print("_handle_http_get request headers: {}".format(request_headers))

        parameters = {}

        # make rest call
        ret_val, response = self._make_rest_call(location, action_result,
                                                method="get", params=parameters, auth=HttpNtlmAuth(username, password), headers=request_headers)

        self.debug_print("ret_val: {}".format(ret_val))
        self.debug_print("response: {}".format(response))

        # response = requests.get(self._base_url + location, auth=HttpNtlmAuth(username, password), verify=False, headers=request_headers)
        # rest call was successful
        data_response = {}
        if ret_val:
            data_response['method'] = 'GET'
            data_response['parsed_response_body'] = response

            self.debug_print("return struct:")
            self.debug_print(action_result)
        # self.debug_print(response.content)
        # self.debug_print(response.raw)
        # self.debug_print(response.headers)
        # self.debug_print(response.request_headers)

        # self.debug_print("text: {}".format(response.text))

        # if response.status_code
            action_result.add_data(data_response)
        # action_result.update_data(data_response)

    def _handle_http_post(self, param):
        self.save_progress("In action handler for: {0}".format(self.get_action_identifier()))

        # add return struction
        action_result = self.add_action_result(ActionResult(dict(param)))

        # get the url (optional)
        location = param.get('location', "")

        # get the request body (required)
        request_body = param['body']
        self.debug_print("request_body: {}".format(request_body))

        payload = {}
        if request_body is not None:
            try:
                payload = json.loads(request_body)
            except:
                # TODO: Throw this exception back to the UI via actionresult
                pass

        self.debug_print(payload)
        # get the username
        # username = param['username']
        username = self.get_config().get('username')

        # get the password
        # password = param['password']
        password = self.get_config().get('password')

        request_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        self.debug_print("_handle_http_post request headers: {}".format(request_headers))

        parameters = {}

        # make rest call
        ret_val, response = self._make_rest_call(location, action_result,
                                                method="post", data=json.dumps(payload), params=parameters, auth=HttpNtlmAuth(username, password), headers=request_headers)

        self.debug_print("ret_val: {}".format(ret_val))
        self.debug_print("response: {}".format(response))

        # response = requests.get(self._base_url + location, auth=HttpNtlmAuth(username, password), verify=False, headers=request_headers)
        # rest call was successful
        data_response = {}
        if ret_val:
            data_response['method'] = 'POST'
            data_response['parsed_response_body'] = response

            self.debug_print("return struct:")
            self.debug_print(action_result)
        # self.debug_print(response.content)
        # self.debug_print(response.raw)
        # self.debug_print(response.headers)
        # self.debug_print(response.request_headers)

        # self.debug_print("text: {}".format(response.text))

        # if response.status_code
            action_result.add_data(data_response)
        # action_result.update_data(data_response)

    def handle_action(self, param):

        ret_val = phantom.APP_SUCCESS

        # Get the action that we are supposed to execute for this App Run
        action_id = self.get_action_identifier()

        self.debug_print("action_id", self.get_action_identifier())

        if action_id == 'get_data':
            ret_val = self._handle_http_get(param)

        if action_id == 'post_data':
            ret_val = self._handle_http_post(param)

        return ret_val

    def initialize(self):

        # Load the state in initialize, use it to store data
        # that needs to be accessed across actions
        self._state = self.load_state()

        # get the asset config
        config = self.get_config()

        self._base_url = config.get('base_url')
        if not self._base_url.endswith('/'):
            self._base_url += "/"

        """
        # Access values in asset config by the name

        # Required values can be accessed directly
        required_config_name = config['required_config_name']

        # Optional values should use the .get() function
        optional_config_name = config.get('optional_config_name')
        """

        self._auth_token = None
        # Do login and save token
        # Token does not look to expire....
        # self._token = self._login(config.get('username'), config.get('password'))

        return phantom.APP_SUCCESS

    def finalize(self):

        # Save the state, this data is saved across actions and app upgrades
        self.save_state(self._state)
        return phantom.APP_SUCCESS


if __name__ == '__main__':

    import pudb
    import argparse

    pudb.set_trace()

    argparser = argparse.ArgumentParser()

    argparser.add_argument('input_test_json', help='Input Test JSON file')
    argparser.add_argument('-u', '--username', help='username', required=False)
    argparser.add_argument('-p', '--password', help='password', required=False)

    args = argparser.parse_args()
    session_id = None

    username = args.username
    password = args.password

    if (username is not None and password is None):

        # User specified a username but not a password, so ask
        import getpass
        password = getpass.getpass("Password: ")

    if (username and password):
        try:
            login_url = HTTPNTLMConnector._get_phantom_base_url() + '/login'

            print("Accessing the Login page")
            r = requests.get(login_url, verify=False)
            csrftoken = r.cookies['csrftoken']

            data = dict()
            data['username'] = username
            data['password'] = password
            data['csrfmiddlewaretoken'] = csrftoken

            headers = dict()
            headers['Cookie'] = 'csrftoken=' + csrftoken
            headers['Referer'] = login_url

            print("Logging into Platform to get the session id")
            r2 = requests.post(login_url, verify=False, data=data, headers=headers)
            session_id = r2.cookies['sessionid']
        except Exception as e:
            print("Unable to get session id from the platform. Error: " + str(e))
            exit(1)

    with open(args.input_test_json) as f:
        in_json = f.read()
        in_json = json.loads(in_json)
        print(json.dumps(in_json, indent=4))

        connector = HTTPNTLMConnector()
        connector.print_progress_message = True

        if (session_id is not None):
            in_json['user_session_token'] = session_id
            connector._set_csrf_info(csrftoken, headers['Referer'])

        ret_val = connector._handle_action(json.dumps(in_json), None)
        print(json.dumps(json.loads(ret_val), indent=4))

    exit(0)
